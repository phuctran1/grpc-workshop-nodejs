const grpc = require('grpc');

const calculatorService = require('./protos/calculator_grpc_pb');
const { sum } = require('./controllers');

function main() {
  const server = new grpc.Server();

  server.addService(calculatorService.CalculatorService, {
    sum,
  });

  const port = server.bind('0.0.0.0:30000', grpc.ServerCredentials.createInsecure());
  server.start();
  console.log('Server is running on port: ', port);
}

main();
