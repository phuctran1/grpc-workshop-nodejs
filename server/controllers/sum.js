const calculator = require('../protos/calculator_pb');

function sum(call, callback) {
  const firstNumber = call.request.getFirstNumber();
  const secondNumber = call.request.getSecondNumber();

  console.log('sum called for: ', firstNumber, secondNumber);

  const result = firstNumber + secondNumber;

  const response = new calculator.SumResponse();
  response.setResult(result);

  callback(null, response);
}

module.exports = sum;
