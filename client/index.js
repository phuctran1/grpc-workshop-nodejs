const grpc = require('grpc');

const service = require('./protos/calculator_grpc_pb');
const calculator = require('./protos/calculator_pb');

const client = new service.CalculatorClient('localhost:30000', grpc.credentials.createInsecure());

function callSum(firstNumber, secondNumber) {
  const request = new calculator.SumRequest();
  request.setFirstNumber(firstNumber);
  request.setSecondNumber(secondNumber);

  client.sum(request, (error, response) => {
    if (error) {
      console.error('Sum error', error);
    } else {
      console.log('Sum response', response.getResult());
    }
  });
}


function main() {
  callSum(70, 15);
}

main();
