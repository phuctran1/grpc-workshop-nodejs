install-basic:
	npm i grpc google-protobuf lodash faker

proto-server:
	protoc -I=. ./protos/calculator.proto \
  --js_out=import_style=commonjs,binary:./server \
  --grpc_out=./server \
  --plugin=protoc-gen-grpc=`which grpc_tools_node_protoc_plugin`

proto-client:
	protoc -I=. ./protos/calculator.proto \
  --js_out=import_style=commonjs,binary:./client \
  --grpc_out=./client \
  --plugin=protoc-gen-grpc=`which grpc_tools_node_protoc_plugin`

proto: proto-server proto-client
